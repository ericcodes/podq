import React from 'react';
import {View, Text} from 'react-native';

interface Props {
  // name is who we're greeting
  name: string;
}
export const Item = ({name}) => {
  return (
    <View>
      <Text>Hello {name ? name : 'you.'}</Text>
    </View>
  );
};

export default Item;
